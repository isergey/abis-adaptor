package com.xerox.abis.ruslan.sru;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStream;

/**
 * Search retrieve response test.
 *
 * @author Yauhen Yurko
 * @since 26.08.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/abis-test-context.xml"})
public class UrlUpdateResponseTest {

    @Autowired
    @Qualifier("urlUpdateResponseMarshaller")
    private Jaxb2Marshaller marshaller;

    @Test
    public void test() throws IOException, JAXBException {
        InputStream inputStream = new ClassPathResource("url-update-response.xml").getInputStream();
        Source source = new StreamSource(inputStream);
        UpdateResponseType updateResponseType =
                (UpdateResponseType) ((JAXBElement) marshaller.unmarshal(source)).getValue();
        Assert.assertTrue("0".equals(updateResponseType.getStatus()));
    }

}
