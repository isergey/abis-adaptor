//package com.xerox.abis.ruslan.sru;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.oxm.jaxb.Jaxb2Marshaller;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.xerox.abis.ruslan.RuslanDataSaver;
//import com.xerox.abis.ruslan.RuslanSaveResponse;
//
//import javax.xml.bind.JAXBElement;
//import javax.xml.bind.JAXBException;
//import javax.xml.transform.Source;
//import javax.xml.transform.stream.StreamSource;
//
//import java.io.IOException;
//import java.io.InputStream;
//
///**
// * Search retrieve response test.
// *
// * @author Yauhen Yurko
// * @since 26.08.2013
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"/abis-test-context.xml"})
//public class UrlUpdateTest {
//	private String recordIdentifier = "RU\\SPSTU\\books\\241";
//	private String docushareUrl = "http://ruslan.ru";
//
//    @Autowired
//    @Qualifier("ruslanDataSaver")
//    private RuslanDataSaver saver;
//
//    @Test
//    public void test() throws IOException, JAXBException {
//    	RuslanSaveResponse res = saver.save(recordIdentifier, docushareUrl);
//    	Assert.assertTrue(res.isSuccess());
//    }
//
//}
