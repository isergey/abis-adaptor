package com.xerox.abis.ruslan.sru;

import com.xerox.abis.Bibliography;
import com.xerox.abis.ruslan.RuslanBibliographyConverter;
import org.junit.Assert;
import org.junit.Test;
import sru.Record;
import sru.Response;
import sru.SruException;
import sru.SruXmlFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sergey on 17.12.15.
 */
public class SruXmlFactoryTest {
    @Test
    public void sruResponsetest() throws IOException, SruException {
        InputStream inputStream = new FileInputStream("for_tests/sruresponse.xml");
//        String sruResponse = convertStreamToString(inputStream);
        SruXmlFactory sruXmlFactory = new SruXmlFactory();
        Response response = sruXmlFactory.fromXml(inputStream);
        for (Record record: response.getRecords()) {
            String recordSchema = record.getRecordSchema();
            if (recordSchema == null) {
                continue;
            }
            if (recordSchema.equals("http://ruslan.ru/dcx")) {
                Bibliography bibliography = RuslanBibliographyConverter.getFromRuslanDcx(record);
                Assert.assertNotNull(bibliography);
                System.out.println(bibliography);
            }

        }
        System.out.println(response.getNumberOfRecords());
    }
}
