package com.xerox.abis.ruslan.sru;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Search retrieve response test.
 *
 * @author Yauhen Yurko
 * @since 13.08.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/abis-test-context.xml"})
public class SearchRetrieveResponseTest {

    @Autowired
    @Qualifier("searchRetrieveResponseMarshaller")
    private Jaxb2Marshaller marshaller;

    @Test
    public void test() throws IOException, JAXBException {
        InputStream inputStream = new ClassPathResource("search-retrieve-response.xml").getInputStream();
        Source source = new StreamSource(inputStream);
        SearchRetrieveResponseType searchRetrieveResponseType =
                (SearchRetrieveResponseType) ((JAXBElement) marshaller.unmarshal(source)).getValue();
        List<RecordType> records = searchRetrieveResponseType.getRecords().getRecord();
        Assert.assertTrue(records.size() == 3);
    }
}
