package com.xerox.abis;

import com.xerox.abis.ruslan.RuslanSaveResponse;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.util.List;

/**
 * @author Alex Sikorsky
 * @since 26.02.2010
 */

@ContextConfiguration(locations = { "/abis-test-context.xml" })
public class AbisServiceTest extends AbstractJUnit4SpringContextTests {
//
//    @Autowired
//    private AbisService abisService;
//
//    @Test
//    public void stubtest() {
//    }
//
////    @Test
//    public void ruslanFindServiceTest() {
//        List<Bibliography> bibs = abisService.findInRuslan("Российская академия наук. Библиотека.", "", "");
//        Assert.assertTrue(bibs.size() > 0);
//    }
//
////    @Test
//    public void ruslanSaveServiceTest() {
//        RuslanSaveResponse result = abisService.saveToRuslan("RU\\SPSTU\\books\\241", "http://www.arbicon.ru");
//        Assert.assertTrue(result.isSuccess());
//    }

}
