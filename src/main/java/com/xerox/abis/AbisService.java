/*
 * PROPRIETARY AND CONFIDENTIAL
 */

package com.xerox.abis;

import com.xerox.abis.ruslan.RuslanCommunicatorService;

import com.xerox.abis.ruslan.RuslanSaveResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Abis integration web service.
 *
 * @author Evgeny Morozov
 */
@WebService
@Service
public class AbisService {
    private static final Log LOG = LogFactory.getLog(AbisService.class);
    @Autowired
    private RuslanCommunicatorService ruslanService;

    /**
     * Finds list of bibliography which math searches parameters in ABIS Ruslan.
     *
     * @param author  - book author
     * @param name    - book name
     * @param marking - book marking
     * @return - bibliography list
     */
    @WebMethod
    public List<Bibliography> findInRuslan(@WebParam(name = "author") String author,
                                           @WebParam(name = "name") String name,
                                           @WebParam(name = "mark") String marking,
                                           @WebParam(name = "cql") String cql) {
        return ruslanService.get(author, name, marking, cql);
    }

    /**
     * Save docushare url to Ruslan.
     *
     * @param identifier   - identifier for record
     * @param docushareUrl - docushare id
     */
    @WebMethod
    public RuslanSaveResponse saveToRuslan(@WebParam(name = "code") String identifier,
                                           @WebParam(name = "docushareUrl") String docushareUrl) {
        return ruslanService.set(identifier, docushareUrl);
    }
}
