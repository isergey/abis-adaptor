package com.xerox.abis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Book bibliography description.
 *
 * @author Evgeny Morozov
 */
public class Bibliography {
    /**
     * СУТРС / краткое описание
     */
    private String description = "";

    /**
     * автор
     */
    private String author = "";

    /**
     * заглавие
     */
    private String title = "";

    /**
     * шифр
     */
    private String code = "";

    /**
     * год издания
     */
    private String publicationDate = "";

    /**
     * вид издания
     */
    private String publicationType = "";

    /**
     * Марк
     */
    private String hiddenMark = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getPublicationType() {
        return publicationType;
    }

    public void setPublicationType(String publicationType) {
        this.publicationType = publicationType;
    }

    public String getHiddenMark() {
        return hiddenMark;
    }

    public void setHiddenMark(String hiddenMark) {
        this.hiddenMark = hiddenMark;
    }

    @Override
    public String toString() {
        return "Bibliography{" +
                "description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", publicationDate='" + publicationDate + '\'' +
                ", publicationType='" + publicationType + '\'' +
                ", hiddenMark='" + hiddenMark + '\'' +
                '}';
    }
}
