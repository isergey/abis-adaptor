package com.xerox.abis;


/**
 * Abstract Conversion interface.
 *
 * @param <U>
 * @param <V>
 * @author Alex Sikorsky
 * @since 23.02.2010
 */
public interface Converter<U, V> {

    public V convert(U value);

}
