package com.xerox.abis.ruslan;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Ruslan read request builder.
 *
 * @author Yauhen Yurko
 * @since 13.08.2013
 */
public class RuslanReadRequestBuilder {

    private static final Log LOG = LogFactory.getLog(RuslanReadRequestBuilder.class);

    private static final String QUERY_LOGIC_OPERATOR = "and";

    private String url;
    private String version;
    private String operation;
    private String maximumRecords;
    private String recordSchema;
    private String author;
    private String title;
    private String isbn;
    private String cql;

    public RuslanReadRequestBuilder(String url, String version, String operation,
                                    String maximumRecords, String recordSchema) {
        this.url = url;
        this.version = version;
        this.operation = operation;
        this.maximumRecords = maximumRecords;
        this.recordSchema = recordSchema;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getMaximumRecords() {
        return maximumRecords;
    }

    public void setMaximumRecords(String maximumRecords) {
        this.maximumRecords = maximumRecords;
    }

    public String getRecordSchema() {
        return recordSchema;
    }

    public void setRecordSchema(String recordSchema) {
        this.recordSchema = recordSchema;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getCql() {
        return cql;
    }

    public void setCql(String cql) {
        this.cql = cql;
    }

    public String build() {
        return url + "?" + "version=" + version + "&operation=" + operation + "&query=" + buildQuery()
                + "&maximumRecords=" + maximumRecords + "&recordSchema=" + recordSchema;
    }

    private String buildQuery() {
        List<String> queryParts = new ArrayList<String>();
        String queryPart = buildQueryItem("dc.title", title);
        if (!StringUtils.isEmpty(queryPart)) {
            queryParts.add(queryPart);
        }

        queryPart = buildQueryItem("dc.creator", author);
        if (!StringUtils.isEmpty(queryPart)) {
            queryParts.add(queryPart);
        }

        queryPart = buildQueryItem("dc.identifier", isbn);
        if (!StringUtils.isEmpty(queryPart)) {
            queryParts.add(queryPart);
        }

        String query = StringUtils.join(queryParts, " and ");
        if (query.isEmpty()) {
            query = cql;
        }
        LOG.info("Request query: " + query);
        try {
            return URLEncoder.encode(StringUtils.defaultString(query).trim(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOG.error("Failed to encode query: " + query);
            throw new IllegalStateException("Failed to build ruslan request");
        }
    }

//    private String buildQueryItem(String name, String value) {
//        return !StringUtils.isEmpty(value) ? name + " any \"" + replaceValue(value) + "\" "
//                + QUERY_LOGIC_OPERATOR + " " : "";
//    }

    private String buildQueryItem(String name, String value) {
        if (StringUtils.isEmpty(value)) {
            return "";
        }
        if (value.endsWith("\"") && value.startsWith("\"")) {
            return name + "=" + replaceValue(value);
        }
        List<String> valueParts = new ArrayList<String>();
        for (String valuePart: value.split("\\s+")) {
            if (StringUtils.isEmpty(value)) {
                continue;
            }
            valueParts.add(replaceValue(valuePart));
        }

        return name + "=" + "(" + StringUtils.join(valueParts, " and ") + ")";
    }
    private String replaceValue(String value) {
        //replace all \ to \\ (accordingly CQL specification)
        return value.replaceAll("\\\\", "\\\\\\\\");
    }
}
