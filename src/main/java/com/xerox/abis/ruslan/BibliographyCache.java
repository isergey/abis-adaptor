package com.xerox.abis.ruslan;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.xerox.abis.Bibliography;
import com.xerox.abis.BibliographyMeta;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Component
public class BibliographyCache {
    private Cache<String, BibliographyMeta> cache = CacheBuilder.newBuilder()
             .maximumSize(1024)
            .build();

    public void put(String key, BibliographyMeta bibliography) {
        cache.put(key, bibliography);
    }

    public BibliographyMeta get(String key) {
        return cache.asMap().get(key);
    }
}
