package com.xerox.abis.ruslan;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.*;
import java.util.Scanner;

/**
 * Ruslan data reader.
 * 
 * @author Yauhen Yurko
 * @since 26.08.2013
 */
public class RuslanDataSaver {

	private static final Log LOG = LogFactory
			.getLog(RuslanReadRequestBuilder.class);
	private static final String SUCCESS_RESPONSE_STATUS = "1";
	private String authUrl;
	private String uid;
	private String password;

	@Autowired
	@Qualifier("urlUpdateResponseMarshaller")
	private Jaxb2Marshaller marshaller;

	private RuslanSaveRequestBuilder builder;

	public RuslanDataSaver() {
		HttpURLConnection.setFollowRedirects(true);
		CookieHandler.setDefault(new CookieManager(null,
				CookiePolicy.ACCEPT_ALL));
	}

	public void setBuilder(RuslanSaveRequestBuilder builder) {
		this.builder = builder;
	}

	public void setAuthUrl(String authUrl) {
		this.authUrl = authUrl;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public RuslanSaveResponse save(String identifier, String docushareUrl, String database) {
		builder.setIdentifier(StringUtils.defaultString(identifier));
		builder.setDocushareUrl(StringUtils.defaultString(docushareUrl));
		builder.setDatabase(database);

		if (!authenticate()) {
			String msg = "Unable to authenticate with credentials provided";
			LOG.error(msg + ": authUrl=" + authUrl + ", uid=" + uid + ", password=" + "password");
			return new RuslanSaveResponse(false, msg);
		}

		URL url;
		try {
			url = new URL(builder.build());
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("PUT");
			OutputStreamWriter out = new OutputStreamWriter(
					connection.getOutputStream());
			out.write(docushareUrl);
			out.close();

			int status = connection.getResponseCode();
			if (status >= 200 && status < 300) {
				return new RuslanSaveResponse(true, "");
			} else {
				Scanner s = new java.util.Scanner(connection.getErrorStream())
						.useDelimiter("\\A");
				String msg = s.hasNext() ? s.next() : "";
				s.close();
				return new RuslanSaveResponse(false, msg);
			}
		} catch (Exception e) {
			LOG.error("Unable to update record: " + e.getMessage());
			return new RuslanSaveResponse(false, e.getMessage());
		}
	}

	private boolean authenticate() {
		try {
			URL auth = new URL(authUrl);
			HttpURLConnection connection = (HttpURLConnection) auth
					.openConnection();
			String credentials = uid + ":" + password;
			connection.setRequestProperty(
					"Authorization",
					"Basic "
							+ DatatypeConverter.printBase64Binary(credentials
									.getBytes("UTF-8")));
			int status = connection.getResponseCode();
			return status >= 200 && status < 300;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

}
