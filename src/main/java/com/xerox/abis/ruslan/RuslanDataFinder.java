package com.xerox.abis.ruslan;

import com.google.common.io.ByteStreams;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sru.SruException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;

/**
 * Ruslan data reader.
 *
 * @author Yauhen Yurko
 * @since 13.08.2013
 */
public class RuslanDataFinder {
    private static final sru.SruXmlFactory sruXmlFactory = new sru.SruXmlFactory();
    private static final Log LOG = LogFactory.getLog(RuslanReadRequestBuilder.class);

    @Autowired
    @Qualifier("searchRetrieveResponseMarshaller")
    private Jaxb2Marshaller marshaller;

    private RuslanReadRequestBuilder builder;

    public void setBuilder(RuslanReadRequestBuilder builder) {
        this.builder = builder;
    }

    public List<sru.Record> find(String author, String title, String isbn, String cql) {

        builder.setAuthor(StringUtils.defaultString(author));
        builder.setTitle(StringUtils.defaultString(title));
        builder.setIsbn(StringUtils.defaultString(isbn));
        builder.setCql(StringUtils.defaultString(cql));
        InputStream inputStream;
        try {
            URL url = new URL(builder.build());
            URLConnection connection = url.openConnection();
            inputStream = connection.getInputStream();
        } catch (IOException e) {
            LOG.error("Failed to get input stream for url: " + builder.build());
            return Collections.emptyList();
        }
        sru.Response sruResponse;
        try {
            sruResponse = sruXmlFactory.fromXml(inputStream);
        } catch (SruException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
        String diagnostic = sruResponse.getDiagnostic();
        if (diagnostic != null & sruResponse.getDiagnostic().length() > 0) {
            LOG.error(diagnostic);
        }
        if (sruResponse.getRecords().size() == 0 || sruResponse.getNumberOfRecords() == 0) {
            return Collections.emptyList();
        }
        return sruResponse.getRecords();
    }
}
