package com.xerox.abis.ruslan;

/**
 * Ruslan save response.
 *
 * @author Yauhen Yurko
 * @since 27.08.2013
 */
public class RuslanSaveResponse {

    private boolean success;
    private String errorMessage;

    public RuslanSaveResponse() {
    }

    public RuslanSaveResponse(boolean success, String errorMessage) {
        this.success = success;
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
