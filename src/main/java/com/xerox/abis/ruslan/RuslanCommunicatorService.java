package com.xerox.abis.ruslan;

import com.xerox.abis.Bibliography;
import com.xerox.abis.BibliographyMeta;
import com.xerox.abis.Converter;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sru.Record;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Ruslan communicator.
 *
 * @author Yauhen Yurko
 * @since 13.08.2013
 */
@Service
public class RuslanCommunicatorService {

    @Autowired
    private RuslanDataFinder finder;

    @Autowired
    private RuslanDataSaver saver;

    @Autowired
    private BibliographyCache bibliographyCache;

    @Autowired
    @Qualifier("ruslanBibliographyConverter")
    private Converter<Record, BibliographyMeta> converter;

    public List<Bibliography> get(String author, String title, String isbn, String cql) {
        if (StringUtils.isEmpty(author) && StringUtils.isEmpty(title) && StringUtils.isEmpty(isbn) && StringUtils.isEmpty(cql)) {
            return Collections.emptyList();
        }

        List<Bibliography> result = new ArrayList<Bibliography>();

        List<Record> records = finder.find(author, title, isbn, cql);
        for (Record record : records) {
            BibliographyMeta bibliographyMeta = converter.convert(record);
            if (bibliographyMeta != null) {
                Bibliography bibliography = bibliographyMeta.getBibliography();
                result.add(bibliography);
                bibliographyCache.put(bibliography.getCode(), bibliographyMeta);
            }
        }

        return result;
    }

    public RuslanSaveResponse set(String identifier, String docushareUrl) {
        BibliographyMeta bibliographyMeta = bibliographyCache.get(identifier);
        if (bibliographyMeta == null) {
            RuslanSaveResponse ruslanSaveResponse = new RuslanSaveResponse();
            ruslanSaveResponse.setSuccess(false);
            ruslanSaveResponse.setErrorMessage("Unable to resolve database of the record " + identifier + ". Find the record before saving");
            return ruslanSaveResponse;
        }
        String database = bibliographyMeta.getDatabase();
        if (database == null || bibliographyMeta.getDatabase().length() == 0) {
            RuslanSaveResponse ruslanSaveResponse = new RuslanSaveResponse();
            ruslanSaveResponse.setSuccess(false);
            ruslanSaveResponse.setErrorMessage("Database not specified in record " + identifier + ". Find the record before saving");
            return ruslanSaveResponse;
        }
        return saver.save(identifier, docushareUrl, database);
    }
}
