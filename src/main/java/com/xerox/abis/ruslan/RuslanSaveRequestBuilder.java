package com.xerox.abis.ruslan;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Ruslan save request builder.
 *
 * @author Yauhen Yurko
 * @since 26.08.2013
 */
public class RuslanSaveRequestBuilder {

    private String url;
    private String identifier;
    private String docushareUrl;
    private String database;

    public RuslanSaveRequestBuilder(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDocushareUrl() {
        return docushareUrl;
    }

    public void setDocushareUrl(String docushareUrl) {
        this.docushareUrl = docushareUrl;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String build() throws UnsupportedEncodingException, URISyntaxException {
        return new URI(url + "/" + URLEncoder.encode(database, "UTF-8") + "/" + URLEncoder.encode(identifier, "UTF-8") + "/marc.856$u").normalize().toString();
    }
}
