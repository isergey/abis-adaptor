package com.xerox.abis.ruslan;

import com.xerox.abis.Bibliography;
import com.xerox.abis.BibliographyMeta;
import com.xerox.abis.Converter;
import com.xerox.abis.ruslan.sru.Desc;
import com.xerox.abis.ruslan.sru.DescriptionType;
import com.xerox.abis.ruslan.sru.RecordType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import sru.Record;
import sru.Utils;

import javax.annotation.Resource;
import java.util.Properties;

/**
 * Ruslan bibliography converter.
 *
 * @author Yauhen Yurko
 * @since 15.08.2013
 */
@Service
@Qualifier("ruslanBibliographyConverter")
public class RuslanBibliographyConverter implements Converter<Record, BibliographyMeta> {

    private static final Log LOG = LogFactory.getLog(RuslanBibliographyConverter.class);
    @Resource(name = "publicationTypes")
    private Properties publicationTypes;

//    public Bibliography convert(RecordType value) {
//        DescriptionType record = value.getRecordData().getDescription();
//        if (record == null) {
//            return null;
//        }
//        Bibliography bibliography = new Bibliography();
//        bibliography.setTitle(StringUtils.defaultString(record.getTitle()));
//        bibliography.setAuthor(StringUtils.defaultString(record.getCreator()));
//        bibliography.setPublicationDate(StringUtils.defaultString(record.getDate()));
//        bibliography.setCode(StringUtils.defaultString(record.getIdentifier()));
//        bibliography.setPublicationType(StringUtils.defaultString(getPublicationType(record.getType())));
//        bibliography.setDescription(StringUtils.EMPTY);
//
//        Desc description = record.getDescription();
//        if (description != null) {
//            bibliography.setDescription(description.getValue());
//        }
//
//        return bibliography;
//    }


    public BibliographyMeta convert(Record record) {
        String recordSchema = record.getRecordSchema();
        if (recordSchema == null) {
            LOG.error("Record no have schema");
            return  null;
        }

        if (recordSchema.equals("http://ruslan.ru/dcx")) {
            return getFromRuslanDcx(record);
        } else {
            LOG.error("Record schema can't be processed: " + recordSchema);
        }
        return null;
    }

    public static BibliographyMeta getFromRuslanDcx(Record record) {
        Bibliography bibliography = new Bibliography();
        NodeList descriptions =  record.getRecordData().getElementsByTagName("rdf:Description");
        if (descriptions.getLength() > 0) {
            Element description = (Element) descriptions.item(0);
            bibliography.setTitle(Utils.getTextFromFirstNode(description, "dc:title"));
            bibliography.setAuthor(Utils.getTextFromFirstNode(description, "dc:creator"));
            bibliography.setPublicationDate(Utils.getTextFromFirstNode(description, "dc:date"));
            bibliography.setCode(record.getRecordIdentifier());
            bibliography.setPublicationType(Utils.getTextFromFirstNode(description, "dc:type"));
            bibliography.setDescription(Utils.getTextFromFirstNode(description, "dc:description"));
        } else {
            LOG.error("Record data element not have rdf:Description tag");
            return null;
        }
        BibliographyMeta bibliographyMeta = new BibliographyMeta();
        bibliographyMeta.setBibliography(bibliography);
        Element extraRecordData = record.getExtraRecordData();
        if (extraRecordData != null) {
            NodeList databaseName =  extraRecordData.getElementsByTagName("databaseName");
            if (databaseName.getLength() > 0) {
                bibliographyMeta.setDatabase(databaseName.item(0).getTextContent());
            }
        }

        return bibliographyMeta;
    }

    private String getPublicationType(String typeCode) {
        if (StringUtils.isEmpty(typeCode)) {
            return null;
        }

        try {
            return publicationTypes.getProperty("publication.type." + typeCode);
        } catch (Exception e) {
            LOG.error("Failed to get publication type for \"" + typeCode + "\"");
            return null;
        }
    }


}
