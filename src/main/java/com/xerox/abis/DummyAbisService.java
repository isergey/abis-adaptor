package com.xerox.abis;

import com.xerox.abis.ruslan.RuslanSaveResponse;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.LinkedList;
import java.util.List;

/**
 * Abis integration web service that always returns hardcoded value.
 *
 * @author Vladimir Yushkevich
 * @since 05.01.12
 */
@WebService
@Service
public class DummyAbisService extends AbisService {
    @Override
    @WebMethod
    public List<Bibliography> findInRuslan(@WebParam(name = "author") String author,
                                           @WebParam(name = "name") String name,
                                           @WebParam(name = "mark") String marking,
                                           @WebParam(name = "cql") String cql) {
        List<Bibliography> bibliographies = new LinkedList<Bibliography>();
        bibliographies.add(getDummyBibliography());
        bibliographies.add(getDummyBibliography());
        return bibliographies;
    }

    private Bibliography getDummyBibliography() {
        Bibliography bibliography = new Bibliography();
        bibliography.setDescription("Dummy Bibliography Description");
        bibliography.setTitle("Dummy Bibliography Title");
        bibliography.setCode("Dummy Bibliography Code");
        return bibliography;
    }

    @Override
    @WebMethod
    public RuslanSaveResponse saveToRuslan(@WebParam(name = "code") String identifier,
                                           @WebParam(name = "docushareUrl") String docushareUrl) {
        return super.saveToRuslan(identifier, docushareUrl);
    }
}
