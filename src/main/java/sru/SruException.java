package sru;

public class SruException  extends  Exception{
    public SruException() {
        super();
    }

    public SruException(String message) {
        super(message);
    }

    public SruException(String message, Throwable cause) {
        super(message, cause);
    }

    public SruException(Throwable cause) {
        super(cause);
    }
}
