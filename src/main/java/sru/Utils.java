package sru;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

/**
 * Created by sergey on 17.12.15.
 */
public class Utils {
    public static String nodeToString(Element node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }

    public static String getTextFromFirstNode(Element element, String name) {
        NodeList tags = element.getElementsByTagName(name);
        if (tags.getLength() > 0) {
            return tags.item(0).getTextContent();
        }
        return "";
    }
    public static String getTextFromFirstNode(Element element, String namespace, String name) {
        NodeList tags = element.getElementsByTagNameNS(namespace, name);
        if (tags.getLength() > 0) {
            return tags.item(0).getTextContent();
        }
        return "";
    }
}
