package sru;


import org.w3c.dom.Element;

public class Record {
    private String recordIdentifier = "";
    private String recordSchema = "";
    private String recordPacking = "";
    private long recordPosition;
    private Element recordData;
    private Element extraRecordData;

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public String getRecordSchema() {
        return recordSchema;
    }

    public void setRecordSchema(String recordSchema) {
        this.recordSchema = recordSchema;
    }

    public String getRecordPacking() {
        return recordPacking;
    }

    public void setRecordPacking(String recordPacking) {
        this.recordPacking = recordPacking;
    }

    public long getRecordPosition() {
        return recordPosition;
    }

    public void setRecordPosition(long recordPosition) {
        this.recordPosition = recordPosition;
    }

    public Element getRecordData() {
        return recordData;
    }

    public void setRecordData(Element recordData) {
        this.recordData = recordData;
    }

    public Element getExtraRecordData() {
        return extraRecordData;
    }

    public void setExtraRecordData(Element extraRecordData) {
        this.extraRecordData = extraRecordData;
    }
}
