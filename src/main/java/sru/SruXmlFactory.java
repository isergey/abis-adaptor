package sru;

import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class SruXmlFactory {
    private static DocumentBuilder builder;
    private static final String sruSchemaUri = "http://docs.oasis-open.org/ns/search-ws/sruResponse";
    private String sruElementPrefix = "";
    public SruXmlFactory() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public Response fromXml(InputStream inputStream) throws SruException {
        Response response = new Response();
        InputSource inputSource = new InputSource(inputStream);
        Document sruResponseDocument;
        try {
            sruResponseDocument = builder.parse(inputSource);
        } catch (Exception e) {
            throw new SruException(e);
        }
        sruResponseDocument.getDocumentElement().normalize();
//        NamedNodeMap attributes = sruResponseDocument.getDocumentElement().gegetAttributes();
//        if (attributes != null) {
//            for (int i = 0; i < attributes.getLength(); i++) {
//                Node attribute = attributes.item(i);
//                String attrName = attribute.getNodeName();
//                String attrValue = attribute.getTextContent();
//                if (attrValue.equalsIgnoreCase("http://docs.oasis-open.org/ns/search-ws/sruResponse")) {
//                    if (attrValue.f
//                }
//                System.out.println(attrName.split(":").length);
//            }
//        }
        response.setNumberOfRecords(getNumberOfRecords(sruResponseDocument));
        response.setRecords(getRecords(sruResponseDocument));
        response.setDiagnostic(getDiagnostic(sruResponseDocument));
        return response;
    }

    private static String getDiagnostic(Document sruResponseDocument) {
        String message = "";
        NodeList diagnosticsNodes = sruResponseDocument.getElementsByTagNameNS(sruSchemaUri, "diagnostics");
        if (diagnosticsNodes.getLength() > 0) {
            message = diagnosticsNodes.item(0).getTextContent();
        }
        return message;
    }
    private static int getNumberOfRecords(Document sruResponseDocument) throws SruException {
        int numberOfRecords = 0;
        NodeList numberOfRecordsNodes = sruResponseDocument.getElementsByTagNameNS(sruSchemaUri, "numberOfRecords");
        if (numberOfRecordsNodes.getLength() > 0) {
            String content = numberOfRecordsNodes.item(0).getTextContent();
            if (!content.isEmpty()) {
                try {
                    numberOfRecords = Integer.parseInt(content);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return numberOfRecords;
    }

    private static List<Record> getRecords(Document sruResponseDocument) {
        List<Record> records = new ArrayList<Record>();
        NodeList recordsNodes = sruResponseDocument.getElementsByTagNameNS(sruSchemaUri, "records");
        if (recordsNodes.getLength() > 0) {
            Element recordNodes = (Element) recordsNodes.item(0);
            NodeList recordNode = recordNodes.getElementsByTagNameNS(sruSchemaUri, "record");
            for (int i = 0; i < recordNode.getLength(); i++) {
                Node record = recordNode.item(i);
                records.add(getSruRecord(record));
            }
        }
        return records;
    }

    private static Record getSruRecord(Node sruRecordNode) {
        Element sruRecordElement = (Element) sruRecordNode;
        Record record = new Record();
        record.setRecordSchema(Utils.getTextFromFirstNode(sruRecordElement, sruSchemaUri, "recordSchema"));
        record.setRecordIdentifier(Utils.getTextFromFirstNode(sruRecordElement, sruSchemaUri, "recordIdentifier"));
        try {
            record.setRecordPosition(Long.parseLong(Utils.getTextFromFirstNode(sruRecordElement, sruSchemaUri, "recordPosition")));
        } catch (NumberFormatException e) {

        }
        NodeList recordDataNodes = sruRecordElement.getElementsByTagNameNS(sruSchemaUri, "recordData");
        if (recordDataNodes.getLength() > 0) {
            record.setRecordData((Element) recordDataNodes.item(0));
        }

        NodeList recordExtraDataNodes = sruRecordElement.getElementsByTagNameNS(sruSchemaUri, "extraRecordData");
        if (recordExtraDataNodes.getLength() > 0) {
            record.setExtraRecordData((Element) recordExtraDataNodes.item(0));
        }
        return record;
    }
    private static Node getChildFirstNodeByName(Element element, String name) {
        NodeList children = element.getElementsByTagName(name);
        if (children.getLength() > 0) {
            return children.item(0);
        }
        return null;
    }
}
